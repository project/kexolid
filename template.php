<?php

/**
 * Return a themed breadcrumb trail.
 *
 * @param $breadcrumb
 *   An array containing the breadcrumb links.
 * @return a string containing the breadcrumb output.
 */
function phptemplate_breadcrumb($breadcrumb) {
  if (!empty($breadcrumb)) {
    return '<div class="breadcrumb">'. implode(' ›› ', $breadcrumb) .'</div>';
  }
}

/**
 * Allow themable wrapping of all comments.
 */
function phptemplate_comment_wrapper($content, $type = null) {
  static $node_type;
  if (isset($type)) $node_type = $type;

  if (!$content || $node_type == 'forum') {
    return '<div id="hozzaszolasok">'. $content . '</div>';
  }
  else {
    return '<div id="hozzaszolasok"><h2 class="hozzaszolasok">'. t('Comments') .'</h2>'. $content .'</div>';
  }
}

/**
 * Override or insert PHPTemplate variables into the templates.
 */
function _phptemplate_variables($hook, $vars) {
  if ($hook == 'page') {

    if ($secondary = menu_secondary_local_tasks()) {
      $output = '<span class="clear"></span>';
      $output .= "<ul class=\"tabs secondary\">\n". $secondary ."</ul>\n";
      $vars['tabs2'] = $output;
    }

    // Hook into color.module
    if (module_exists('color')) {
      _color_page_alter($vars);
    }
    
    return $vars;
  }
  return array();
}

/**
 * Returns the rendered local tasks. The default implementation renders
 * them as tabs.
 *
 * @ingroup themeable
 */
function phptemplate_menu_local_tasks() {
  $output = '';

  if ($primary = menu_primary_local_tasks()) {
    $output .= "<ul class=\"tabs primary\">\n". $primary ."</ul>\n";
  }

  return $output;
}

function kexy_szolid_smink_links($links, $attributes = array('class' => 'links'))
{
  $output = '';

  if (count($links) > 0) {
    $output = '<ul'. drupal_attributes($attributes) .'>';

    $num_links = count($links);
    $i = 1;

    foreach ($links as $key => $link) {
      $class = '';
      $link['title'] = '<span>' . $link['title'] . '</span>';
      // Automatically add a class to each link and also to each LI
      if (isset($link['attributes']) && isset($link['attributes']['class'])) {
        $link['attributes']['class'] .= ' ' . $key;
        $class = $key;
      }
      else {
        $link['attributes']['class'] = $key;
        $class = $key;
      }

      // MEG KÉNE NÉZNI, HOGY AZ ACTIVE attribútum hozzá van-e fűzve, és ha igen, levágni, és külön hozzáadni
      $aktiv = strpos($class, '-active');
      // na meg is néztem :)

      // Add first and last classes to the list of links to help out themers.
      $extra_class = '';
      if ($i == 1) {
        $extra_class .= 'first ';
      }
      if ($i == $num_links) {
        $extra_class .= 'last ';
      }
      //itt meg ha aktív, akkor hozzáadom a class-t külön is
      if ($aktiv !== false) {
        $extra_class .= 'active ';
      }
      

      
      $output .= '<li class="'. $extra_class . $class .'">';

      // Is the title HTML?
      $html = isset($link['html']) && $link['html'];

      // Initialize fragment and query variables.
      $link['query'] = isset($link['query']) ? $link['query'] : NULL;
      $link['fragment'] = isset($link['fragment']) ? $link['fragment'] : NULL;

      if (isset($link['href'])) {
        $output .= l($link['title'], $link['href'], $link['attributes'], $link['query'], $link['fragment'], FALSE, TRUE);
      }
      else if ($link['title']) {
        //Some links are actually not links, but we wrap these in <span> for adding title and class attributes
        if (!$html) {
          $link['title'] = check_plain($link['title']);
        }
        $output .= '<span'. drupal_attributes($link['attributes']) .'>'. $link['title'] .'</span>';
      }

      $i++;
      $output .= "</li>\n";
    }

    $output .= '</ul>';
  }

  return $output;
}

function kexy_szolid_smink_datum($timestamp)
{
  $output = '';

  $ev = t(gmdate('Y', $timestamp));
  $ho = t(gmdate('m', $timestamp));
  $nap = t(gmdate('d', $timestamp));
  $ora = t(gmdate('H', $timestamp));
  $perc = t(gmdate('i', $timestamp));
  
  $output .= '<p class="datum">' . $ev . '. ' . $ho . '. ' . $nap . '. -- ' . $ora . ' : ' . $perc . '</p>';

  return $output;
}

function kexy_szolid_smink_username($object)
{
  if ($object->uid && $object->name) {
    global $user;
    $account = user_load(array('uid'=>$object->uid));
    if (module_exists('profile')) {
       if ($account->profile_nickname) {
         $name = $account->profile_nickname;
       }
    } elseif (drupal_strlen($object->name) > 20) {
      $name = drupal_substr($object->name, 0, 15) .'...';
    } else {
      $name = $object->name;
    }
    
    if (user_access('access user profiles')) {
      $output = l($name, 'user/'. $object->uid, array('title' => t('View user profile.')));
    } else {
      $output = check_plain($name);
    }
  } else {
    $output = variable_get('anonymous', t('Anonymous'));
  }

  return $output;
}

function kexy_szolid_smink_event_more_link($path) {}
//function kexy_szolid_smink_xml_icon(){}
function kexy_szolid_smink_feed_icon(){}
function kexy_szolid_smink_event_ical_link($path) {}

function kexy_szolid_smink_forum_new($tid) {
  global $user;

  $sql = "SELECT n.nid FROM {node} n LEFT JOIN {history} h ON n.nid = h.nid AND h.uid = %d INNER JOIN {term_node} r ON n.nid = r.nid AND r.tid = %d WHERE n.status = 1 AND n.type = 'forum' AND h.nid IS NULL AND n.created > %d ORDER BY created";
  $sql = db_rewrite_sql($sql);
  $nid = db_result(db_query_range($sql, $user->uid, $tid, NODE_NEW_LIMIT, 0, 1));

  return $nid ? $nid : 0;
}

function kexy_szolid_smink_uj_forumbejegyzes($tid) {
  $output = '';

  if (kexy_szolid_smink_forum_new($tid)) {
    $output .= '<span style="font-weight: bold; color: #f00;">(friss)</span>';
  }
  
  return $output;
}

// a függvény kilistázza az első paraméterben megkapott taxonómia szótár elemeit, és amennyiben annak csak bizonyos mélységeit szeretnénk listázni, meg kell adnunk a második paraméterben, hogy melyik szintet
// pl. a második szinten levő elemek listázásához a 2 paramétert kell átadni
function kexy_szolid_smink_kategorialista($szotar_id, $listakent = false,$listazando_szint = 0, $url = 'taxonomy/term')
{
  $output = '';

  $taxonomy_array = taxonomy_get_tree($szotar_id);

  if (empty($taxonomy_array)) { return ''; }
  
  if (($listakent == 'true') || ($listakent == 'lista') || ($listakent == 'list') || ($listakent == 'listakent') || ($listakent == 'listaba')) {
    $listakent = true;
  } else {
    $listakent = false;
  }
  
  $output .= $listakent ? '<ul>' : '';

  foreach ($taxonomy_array as $key => $value) {
    if ($listazando_szint != 0) {
      if ($value->depth == ($listazando_szint - 1)) {
        $tid = $value->tid;
        $taxonomy_name = $value->name;

        $output .= ($listakent ? '<li class="collapsed">' : '') . l($taxonomy_name . kexy_szolid_smink_uj_forumbejegyzes($tid), drupal_get_path_alias("$url/$tid")) . ($listakent ? '</li>' : '') . '<br />';
      }
    } else {
      $tid = $value->tid;
      $taxonomy_name = $value->name;

      $output .= ($listakent ? '<li class="collapsed">' : '') . l($taxonomy_name, drupal_get_path_alias("$url/$tid")) . ($listakent ? '</li>' : '') . '<br />';
    }
  }
  
  $output .= $listakent ? '</ul>' : '';
  
  return $output;

  /*
  print l('Ezüst ékszerek', drupal_get_path_alias("node/1")) . '<br />';
  print l('Egyedi gyártású ékszerek', drupal_get_path_alias("node/2")) . '<br />';
  print l('Ékszerek javítása', drupal_get_path_alias("node/3"));
  */
}

function kexy_szolid_smink_forum_topic_navigation($node) {}

function kexy_szolid_smink_image_attach_teaser($node) {
  $img_size = variable_get('image_attach_size_teaser_'. $node->type, 'thumbnail');

  if ($img_size != IMAGE_ATTACH_HIDDEN) {
    drupal_add_css(drupal_get_path('module', 'image_attach') .'/image_attach.css');

    $image = node_load($node->iid);
    if (!node_access('view', $image)) {
      // If the image is restricted, don't show it as an attachment.
      return NULL;
    }
    $info = image_get_info(file_create_path($image->images[$img_size]));

    $output = '<div style="width: '. $info['width'] .'px" class="image-attach-teaser">';
    
    $content = node_load($node->nid);
    $munka_url = $content->field_munka_web_url[0]['value'];
    
    if ($node->type == 'munka_weboldal') {
      if ($munka_url) {
        $output .= l(image_display($image, $img_size), $munka_url, array(), NULL, NULL, FALSE, TRUE);
      }
    } else {
      $output .= l(image_display($image, $img_size), "node/$node->iid", array(), NULL, NULL, FALSE, TRUE);
    }
    
    
    $output .= '</div>'."\n";

    return $output;
  }
}
