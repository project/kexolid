<?php phptemplate_comment_wrapper(NULL, $node->type); ?>

<div id="node-<?php print $node->nid; ?>" class="node<?php if ($sticky) { print ' sticky'; } ?><?php if (!$status) { print ' node-unpublished'; } ?>">

<?php print $picture ?>

  <?php if ($submitted): ?>
    <div class="bejegyezve">
      <?php print t('!date', array('!date' => theme('datum', $node->created))); ?>
      <!-- <p class="szerzo">Írta: <?php // print t('!username', array('!username' => theme('username', $node))); ?></p> -->
    </div>
  <?php endif; ?>


<?php if ($page == 0): ?>
  <h2><a href="<?php print $node_url ?>" title="<?php print $title ?>"><?php print $title ?></a></h2>
<?php endif; ?>

  <div class="tartalom">
    <?php print $content ?>
  </div>

  <?php if ($links || $taxonomy) { ?>
  <div class="clear meta">
  <?php } else { ?>
  <div class="clear">
  <?php } ?>
  
    <div class="egyebek">
    <?php if ($taxonomy): ?>
      <div class="cimkek">Címkék: <?php print $terms ?></div>
    <?php endif;?>
    </div>

    <?php if ($links): ?>
      <div class="linkek"><?php print $links; ?></div>
    <?php endif; ?>

  </div>

</div>
