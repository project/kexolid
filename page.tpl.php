<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language ?>" lang="<?php print $language ?>">
  <head>
    <title><?php print $head_title ?></title>
    <?php print $head ?>
    <?php print $styles ?>
    <?php print $scripts ?>
    <link type="text/css" rel="stylesheet" href="<?php print base_path() . path_to_theme() ?>/stilusok/oldal-elrendezes.css" />
    <link type="text/css" rel="stylesheet" href="<?php print base_path() . path_to_theme() ?>/stilusok/oldal-kinezet.css" />
    <link type="text/css" rel="stylesheet" href="<?php print base_path() . path_to_theme() ?>/stilusok/tartalom.css" />
    <link type="text/css" rel="stylesheet" href="<?php print base_path() . path_to_theme() ?>/stilusok/blokk.css" />
    <link type="text/css" rel="stylesheet" href="<?php print base_path() . path_to_theme() ?>/stilusok/komment.css" />
  </head>

  <body>
  
    <a class="rejtett" href="http://www.midnightfun.co.uk/modules/anti_spam/">Egy kellemes link, hiperaktív robotoknak kihagyhatatlan.</a>
    <a class="rejtett" href="http://hiphop-110940277947.spampoison.com">Hip hop forever... Kizárólag droidoknak!</a>

    <div id="tarolo">

      <div id="fejlec">

        <div id="fejlec-felso">

                <?php print $header; ?>

                <?php if (isset($primary_links)) : ?>
                  <?php print theme('links', $primary_links, array('class' => 'links primary-links')) ?>
                <?php endif; ?>

        </div>

        <div id="fejlec-kozepso">

                <?php
                // Prepare header
                $site_fields = array();
                if ($site_name) {
                  $site_fields[] = check_plain($site_name);
                }
                if ($site_slogan) {
                  $site_fields[] = check_plain($site_slogan);
                  print '<div id="szlogen">' . $site_fields[1] . '</div>';
                }
                $site_title = implode(' - ', $site_fields);
                $site_fields[0] = '<span class="title">'. $site_fields[0] .'</span>';
                $site_html = implode(' ', $site_fields);

                if ($logo || $site_title) {
                  if ($logo) {
                    print '<img src="'. $GLOBALS['base_root'] . $logo .'" alt="'. $site_title .'" id="logo" />';
                  }
                  print '<h1><a href="'. check_url($base_path) .'" title="'. $site_title .'" id="oldal-nev">' . $site_fields[0] . '</a></h1>';
                }
              ?>

        </div>

        <div id="fejlec-also"></div>

      </div>

      <div id="tartalmi-resz">

        <div id="oldalsav">

              <?php if ($sidebar_left): ?>
                <div id="bal-oldalsav" class="oldalsav">
                  <?php if ($search_box): ?><div class="block block-theme"><?php print $search_box ?></div><?php endif; ?>
                  <?php print $sidebar_left ?>
                </div>
              <?php endif; ?>


              <?php if ($sidebar_right): ?>
                <div id="jobb-oldalsav" class="oldalsav">
                  <?php if (!$sidebar_left && $search_box): ?><div class="block block-theme"><?php print $search_box ?></div><?php endif; ?>
                  <?php print $sidebar_right ?>
                </div>
              <?php endif; ?>

        </div>

        <?php if (!$sidebar_right && !$sidebar_left) { ?>
        <div id="tartalom-forum">
        <?php } else { ?>
        <div id="tartalom">
        <?php } ?>

              <?php if ($breadcrumb): print $breadcrumb; endif; ?>
              <?php if ($mission): print '<div id="kuldetes">'. $mission .'</div>'; endif; ?>

              <?php if ($tabs): print '<div id="fulek-csomagolo">'; endif; ?>
              <?php if ($title): print '<h2'. ($tabs ? ' class="fulekkel"' : '') .'>'. $title .'</h2>'; endif; ?>
              <?php if ($tabs): print $tabs .'</div>'; endif; ?>

              <?php if (isset($tabs2)): print $tabs2; endif; ?>

              <?php if ($help): print $help; endif; ?>
              <?php if ($messages): print '<div id="uzenetek">' . $messages . '</div>'; endif; ?>
              <?php print $content ?>
              <?php print $feed_icons ?>


        </div>

      </div>

      <div id="lablec">

              <?php print $footer_message ?>

      </div>

    </div>

  </body>
</html>
